package com.stephen.prometheus.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ssc on 2022-03-05 21:31 .
 * Description:
 */
@RestController
public class HelloController {


    @RequestMapping("hello")
    public String hello() {
        return "hello world";
    }


}
